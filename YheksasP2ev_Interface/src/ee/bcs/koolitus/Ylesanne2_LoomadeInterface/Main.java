package ee.bcs.koolitus.Ylesanne2_LoomadeInterface;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		Human human = new Human();
		human.defineSpeciesName("Human");
		human.setGender(Gender.MALE);
		human.setBirthday("25.03.1999");
		human.moveAhead();

		Mammal dolphin = new Dolphin();
		dolphin.defineSpeciesName("Human");
		dolphin.setGender(Gender.MALE);
		dolphin.moveAhead();

		Human humanEq1 = new Human();
		humanEq1.setAddress("Vutisaar");
		humanEq1.setBirthday("24.04.1997");
		humanEq1.setGender(Gender.FEMALE);
		humanEq1.defineSpeciesName("Pipi");

		Human humanEq2 = new Human();
		humanEq2.setAddress("Kurrunurru");
		humanEq2.setBirthday("23.03.1999");
		humanEq2.setGender(Gender.MALE);
		humanEq2.defineSpeciesName("Pipi");

		System.out.println("---------------");
		System.out.println("humanEq1 == humanEq2?: " + humanEq1.equals(humanEq2));
		System.out.println("human == dolphin?: " + human.equals(dolphin));

		System.out.println("-----------");
		System.out.println("humanEq1 räsi == humanEq2 räsi?: " + (humanEq1.hashCode() == humanEq2.hashCode()));

		Set<Human> humans = new TreeSet<>();
		humans.add(humanEq1);
		humans.add(humanEq2);
		System.out.println("Näide1:" + humans.size());
		
		for (Human h: humans) {
			System.out.println("Näide2: " + h);
		}

	}

}
