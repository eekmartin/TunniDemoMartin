package ee.bcs.koolitus.Ylesanne2_LoomadeInterface;

public class Dolphin extends Mammal {

	public void moveAhead() {
		swimAhead();
		swimToSurface();
		breathe();
		dive();
		swimAhead();

	}

	private void swimToSurface() {
		System.out.println(this.speciesName + ", the dolphin, swims to the surface to breathe.");
		System.out.println(this.speciesName + ", the dolphin, dives bach underwater.");
	}

	private void swimAhead() {
		System.out.println(this.speciesName + ", the dolphin, swims ahead.");
	}

	private void breathe() {
		System.out.println(this.speciesName + ", the dolphin, breathes.");
	}

	private void dive() {
		System.out.println("Dolphin dives.");
	}

}
