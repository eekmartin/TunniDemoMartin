package Arvud;

public class Arvud {
	public static void main(String[] args) {
		// see on meetodi deklaratsioon
		int int1 = -9;
		long long1 = 123347483648L;
		byte byte3v = -110;
		Byte byte3 = new Byte(byte3v);
		Byte byte4 = new Byte("-110");
		Byte byte5 = 127;
		// see on meetodi keha

		Byte byte6 = (byte) (byte3 + byte4);
		// see on castimine - anname ette, mis tüüpi vastus tuleb tõlkida
		System.out.println(byte3 + long1);

		Integer int3 = 360000;
		Integer int4 = 1_000_000_000;
		Integer int5 = new Integer("-286");
		
		System.out.println(int5);
		
		Double double1 = (double)(new Integer("-286"));
		System.out.println(double1);

		Double double2 = new Double("-286");
		System.out.println(int5 + double2);
		
		int arv1 = 8;
		boolean kasVordub = arv1 == 8;
		System.out.println("boolean on: "+ kasVordub);
		
		int arv2 = arv1 + 8;
		int arv3 = arv1++;
//siin suurendatakse arv1 väärtust alles siis, kui see väärtus on arvule 3 juba antud
//ehk siis arv3 väärtuseks jääb 8, mitte 9.
		System.out.println("arv1 = " + arv1);
		System.out.println("arv2 = " + arv2);
		System.out.println("arv3 = " + arv3);
		
		int arv4 = 12;
		arv4 +=8;
		System.out.println("arv4 = " + arv4);
		System.out.println("----------");
		
		byte a = 1;
		byte b = 1;
		byte c = 3;
		System.out.println(a==b);
		System.out.println(a==c);
		
		a = c;
		System.out.println(a==b);
		System.out.println(a==c);
		System.out.println("---------");
		
		int x1 = 10;
		int x2 = 20;
		int y1 = ++x1;
		System.out.println(x1);
		System.out.println(y1);
		System.out.println("----------");
		
		int y2 = x2++;
		System.out.println("x2 = " + x2);
		System.out.println("y2 = " + y2);
		System.out.println("----------");
		
		int d = 18%3;
		int e = 19%3;
		int f = 20%3;
		int g = 21%3;
//kui tahad nende tähiseks panna samuti a,b,c.. siis pead uuesti meetodi välja kutsuma
//"	public static void main(String[] args) " jne
		System.out.println("d = " + d + "; e = " + e + "; f = " + f + "; g = " + g);
		System.out.println(d + "; " + e + "; " + f + "; " + g);
		System.out.println("--------");
		
		System.out.println("Isa ütles: \"Tule siia!\"");
	}
}
