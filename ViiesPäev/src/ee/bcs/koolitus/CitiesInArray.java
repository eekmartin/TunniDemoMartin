package ee.bcs.koolitus;

import java.util.Arrays;
import java.util.List;

public class CitiesInArray {
	public static void main(String[] args) {

		String[][] linnadeTabel = new String[195][5];
		String[] tallinn = { "Eesti", "Tallinn", "Tallinn", "Tallinn" };
		String[] helsingi = { "Soome", "Helsinki", "Helsingi", "Helsinki", "Helsingfors" };
		String[] kopenhagen = { "Taani", "Copenhagen", "Kopenhagen", "København" };
		String[] stockholm = { "Rootsi", "Stockholm", "Stockholm", "Stockholm" };
		String[] riia = { "Läti", "Riga", "Riia", "Rīga" };
		String[] varssav = { "Poola", "Warsaw", "Varssav", "Warszawa" };

		linnadeTabel[0] = tallinn;
		linnadeTabel[1] = helsingi;
		linnadeTabel[2] = kopenhagen;
		linnadeTabel[3] = stockholm;
		linnadeTabel[4] = riia;
		linnadeTabel[5] = varssav;

		/* 
		// neljas ülesanne:
		for (int reaNumber = 0; reaNumber < linnadeTabel.length; reaNumber++) {
			if (linnadeTabel[reaNumber][1] != null)
				System.out.println("Eestikeelne nimi: " + linnadeTabel[reaNumber][1]);
		}
		System.out.println("--------------------");

		// viies ülesanne:
		for (int reaNumber = 0; reaNumber < linnadeTabel.length; reaNumber++) {
			if (linnadeTabel[reaNumber][1] != null) {
				System.out.println("Riik - " + linnadeTabel[reaNumber][0] + ": pealinn - " + linnadeTabel[reaNumber][2]
						+ "; inglise keeles - " + linnadeTabel[reaNumber][0] + ", kohalikus keeles: "
						+ linnadeTabel[reaNumber][3]);
			}
		}
		*/
		
		System.out.println("--------------------");
		
		//linnad ilma riiata:
		
		String[][] linnadIlmaRiiata = new String[195][5];
		for(int i = 0; i < linnadeTabel.length; i++) {
			if (i < 2) {
				linnadIlmaRiiata[i] = linnadeTabel[i];
			} else if (i >=2) {
				linnadIlmaRiiata[i] = linnadeTabel[i+1];
			}
		}
		for (int reaNumber = 0; reaNumber < linnadeTabel.length; reaNumber++)
		System.out.println("Riik - " + linnadIlmaRiiata[reaNumber][0] + ": pealinn - " + linnadIlmaRiiata[reaNumber][2]
				+ "; inglise keeles - " + linnadIlmaRiiata[reaNumber][0] + ", kohalikus keeles: "
				+ linnadIlmaRiiata[reaNumber][3] + " või " + linnadIlmaRiiata[reaNumber][4]);
		
		// kuues ülesanne:
		for (int reaNumber = 0; reaNumber < linnadeTabel.length; reaNumber++) {
			if (linnadeTabel[reaNumber].length > 4 && linnadeTabel[reaNumber][4] != null)
				System.out.println("Riik - " + linnadeTabel[reaNumber][0] + ": pealinn - " + linnadeTabel[reaNumber][2]
						+ "; inglise keeles - " + linnadeTabel[reaNumber][0] + ", kohalikus keeles: "
						+ linnadeTabel[reaNumber][3] + " või " + linnadeTabel[reaNumber][4]);
		}

		// liigutan riiki ringi:
		/*
		 * for (String[] linnad : linnadeTabel){ if (linnad[0] !=null) { String[]
		 * ajutineRiik = linnad.clone(); linnad[0] = ajutineRiik [3]; linnad[1] =
		 * ajutineRiik [0]; linnad[2] = ajutineRiik [1]; linnad[3] = ajutineRiik [2]; }
		 * }
		

		System.out.println("Peale riigi esimeseks liigutamist: " + Arrays.deepToString(linnadeTabel));
		System.out.println("-------------------");

		// alternatiiv:
		String[] uusTaani = { "Taani", "Copenhagen", "Kopenhagen", "København", "Koooop" };
		linnadeTabel[1] = uusTaani;

		for (String[] linnad : linnadeTabel) {
			if (linnad[1] != null) {
				StringBuilder localName = new StringBuilder().append(linnad[3]);
				if (linnad.length > 4) {
					for (int i = 4; i < linnad.length; i++) {
						localName.append(", " + linnad[i]);
					}
				}
				System.out.println("Riik - " + linnad[0] + ": pealinn - " + linnad[2] + "; inglise keeles - "
						+ linnad[0] + ", kohalikus keeles: " + localName);
			}
		}

		System.out.println("---------------------");

		// seitsmes ülesanne.
		for (String[] linnad : linnadeTabel) {
			linnad[0] = "country:" + linnad[0];
		}
	}

	private static void printCountriesWithLocalNames(String[][] linnadeTabel) {
		for (String[] linnad : linnadeTabel) {
			if (linnad[1] != null) {
				StringBuilder localName = new StringBuilder().append(linnad[3]);
				if (linnad.length > 4) {
					for (int i = 4; i < linnad.length; i++) {
						localName.append(", " + linnad[i]);
					}
				}
				System.out.println("Riik - " + linnad[0] + ": pealinn - " + linnad[2] + "; inglise keeles - "
						+ linnad[0] + ", kohalikus keeles: " + localName);
			}
			System.out.println("----------------------");
		}
		System.out.println("----------------");
		// kaheksas ülesanne:

		
		for (String[] linnad : linnadeTabel) {
			if (linnad[1] != null) {
				StringBuilder localName = new StringBuilder().append(linnad[3]);
				if (linnad.length > 4) {
					for (int i = 4; i < linnad.length; i++) {
						localName.append(", " + linnad[i]);
					}
				}
				System.out.println("Riik - " + linnad[0].split(":")[1] + ": pealinn - " + linnad[2]
						+ "; inglise keeles - " + linnad[0] + ", kohalikus keeles: " + localName);
			}
		}
		System.out.println("-----------------------");

		// üheksas ülesanne:
		String[] pipi = { "Kurrunurruvutisaare Kuningriik", "Longstocking City", "Pikksuka linn", "Langstrump" };
		String[][] linnad3 = new String[195][5];
		linnad3[2] = pipi;

		for (int riigiIndeks = 0; riigiIndeks <= linnadeTabel.length; riigiIndeks++) {
			if (riigiIndeks < 2) {
				linnad3[riigiIndeks] = linnadeTabel[riigiIndeks];
			} else if (riigiIndeks > 2) {
				linnad3[riigiIndeks] = linnadeTabel[riigiIndeks - 1];
			}
		}
		for (int riigiIndeks = 4; riigiIndeks < linnad3.length; riigiIndeks++) {
			System.out.println("Riik - " + linnad3[riigiIndeks][0] + ": pealinn - " + linnad3[riigiIndeks][2]
					+ "; inglise keeles - " + linnad3[riigiIndeks][1] + ", kohalikus keeles: "
					+ linnad3[riigiIndeks][3]);
			System.out.println("-------------------");
		}
	}
	// kümnes ülesanne:
	*/
	
	}
}