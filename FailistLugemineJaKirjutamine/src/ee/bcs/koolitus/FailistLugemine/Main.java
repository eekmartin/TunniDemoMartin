package ee.bcs.koolitus.FailistLugemine;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader("testFile.txt");
		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: ");
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				System.out.println("Readerit ei saanud sulgeda: " + e.getMessage());
			}

		}
		try (FileReader fileReader1 = new FileReader("testFile.txt")) {
		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: ");
			e.printStackTrace();
		} catch (IOException e1) {
			System.out.println("Readerit ei saanud sulgeda: " + e1.getMessage());
		}
	
		try {
			writeFairyTale("OnceUponATime.txt");
		} catch (FileNameEmptyException e) {
			System.out.println(e.getMessage());
		}
	}
	static private void writeFairyTale(String fileName) throws IOException, FileNameEmptyException {
		if(fileName == null || fileName.equals("")) {
			throw new FileNameEmptyException("Loo kirjutamiseks on faili nime vaja.");
		}
		FileWriter fw = new FileWriter(fileName);
	}
}
