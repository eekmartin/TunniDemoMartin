package ee.bcs.koolitus.autohaldus.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ee.bcs.koolitus.autohaldus.Auto;

public class AutoResource {
	
	
	//java database, mysql tüüpi
	String dbUrl = "jdbc:mysql://localhost:3306/new_schema";
	//defineerin properties, millega andmebaasi ühendus luua:
	Properties connectionProperties = new Properties();
	
	//meetodi välja kutsumine tuleb teha meetodi sees, mitte klassis!
	//teen meetodi private void put ja hakkan connectionisse asju panema
	private void setProperties() {
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "bcskoolitus");
	}

	public List<Auto> getAllAutos() {
		List<Auto> autod = new ArrayList<>();

		// seadistan properties faili,
		// loon andmebaasi ühenduse ja käivitan päringu (vigade püüdmise käsuga):
		setProperties();
		
		//meetodi LoadDriver välja kutsumine klassist DatabaseConnection
		DatabaseConnection.loadDriver();
		// et andmebaasiga suhelda, loon ühenduse java ja mysql andmebaasi vahel (kus
		// asub, mis on parool)
		try (Connection connection = DriverManager.getConnection(dbUrl, connectionProperties)) {
			// stringi sisse kirjutan SQL vormingus päringu (mida ma tahan andmebaasist)
			String sqlQuery = "SELECT * FROM auto";
			// järgnev on kohustuslik - peal looma statement-tüüpi objekti
			Statement statement = connection.createStatement();
			// siin käivitan eelneva päringu:
			ResultSet results = statement.executeQuery(sqlQuery);
			// kuna mu ressursis võib olla mitu rida, pean selle sammhaaval läbi käima:
			while (results.next()) {
				// siin loon uue auto objekti, kuhu panen andmebaasist saadud andmed.
				Auto auto = new Auto().setId(results.getInt("idautohaldus")).setName(results.getString("tootja_nimi"))
						.setMudel(results.getString("mudel"));
				autod.add(auto);
			}
			// siin panen ressursid kinni (tee seda kindlasti!)
			results.close();
			statement.close();

			// catchi lisame selleks, et kui tekib exception,
			// siis püütakse see kinni (ja soovi korral talletatakse:
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return autod;
	}

	public Auto getAutoById(int idautohaldus) {
		Auto auto = null;

		setProperties();
		DatabaseConnection.loadDriver();
		try (Connection connection = DriverManager.getConnection(dbUrl, connectionProperties)) {
			String sqlQuery = "SELECT * FROM auto WHERE idautohaldus = " + idautohaldus;
			Statement statement = connection.createStatement();
			ResultSet results = statement.executeQuery(sqlQuery);
			while (results.next()) {
				auto = new Auto().setId(results.getInt("idautohaldus")).setName(results.getString("tootja_nimi"))
						.setMudel(results.getString("mudel"));
			}
			results.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return auto;
	}


	
	public Auto addAuto(Auto auto) {
		setProperties();
		DatabaseConnection.loadDriver();

		try(Connection connection = DriverManager.getConnection(dbUrl, connectionProperties)) {
			String sqlQuery = "INSERT INTO auto (tootja_nimi, mudel) VALUES('" + auto.getName() + "', '"
					+ auto.getMudel() + "')";
		Statement statement = connection.createStatement();
		statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
		ResultSet results = statement.getGeneratedKeys();
		while(results.next()) {
			auto.setId(results.getInt(1));
		}
		results.close();
		statement.close();
	
} catch (SQLException e) {
	e.printStackTrace();
}
return auto;
	}

	
	
	//Ühe rea kustutamine ID järgi:
	public void deleteAuto(Auto auto) {
		String sqlQuery = "DELETE FROM auto WHERE idautohaldus = " + auto.getId();
		setProperties();
		DatabaseConnection.loadDriver();

		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Some error on delete");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		
		
		public void updateAuto(Auto auto) {
			String sqlQuery = "UPDATE auto SET tootja_nimi = '" + auto.getName() + "', mudel = '" + auto.getMudel()
					+ "' WHERE idautohaldus = " + auto.getId();

			try {
				Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
				if (code != 1) {
					throw new SQLException("Some error on update");
				}
			} catch (SQLException e) {
				System.out.println("Error on running query: " + e.getStackTrace());
			}

	}


	
}
	// tabeli update'imine - vt gitlabist laanekassi koodi, seal olemas


