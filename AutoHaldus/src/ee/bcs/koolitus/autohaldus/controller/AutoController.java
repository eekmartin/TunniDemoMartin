package ee.bcs.koolitus.autohaldus.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.autohaldus.Auto;
import ee.bcs.koolitus.autohaldus.db.AutoResource;

@Path("/auto")
public class AutoController {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Auto> getAllAutos(){
		//hakkan andmebaasist autosid küsima:
		AutoResource autoR = new AutoResource();
		List<Auto> autod = autoR.getAllAutos();
		return autod;		
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	//PathParam on muutuja, mis tuleb pathi sisse (loogelistesse sulgudesse?).
	public Auto getAutoById(@PathParam("id") int idautohaldus) {
		AutoResource autoR = new AutoResource();
		Auto auto = autoR.getAutoById(idautohaldus);
		return auto;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Auto addAuto(Auto uusAuto) {
		AutoResource autoR = new AutoResource();
		Auto auto = autoR.addAuto(uusAuto);
		return auto;
	}
	
	@DELETE
	@Path("/{id}")
	public void deleteAuto(@PathParam("id") int idautohaldus) {
		new AutoResource().deleteAuto(new Auto().setId(idautohaldus));
	
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Auto updateAuto(Auto muudetudAuto) {
		new AutoResource().updateAuto(muudetudAuto);
		return muudetudAuto;
	}

	
}
