package ee.bcs.koolitus.Ylesanne2_Loomad;

public class Main {
	public static void main (String[] args) {
	Mammal human = new Human();
	human.defineSpeciesName("Human");
	human.setGender(Gender.MALE);
	human.moveAhead();
	
	Mammal dolphin = new Dolphin();
	dolphin.defineSpeciesName("Dolphin");
	dolphin.setGender(Gender.FEMALE);
	dolphin.moveAhead();
	dolphin.swimAhead();
	}

}
