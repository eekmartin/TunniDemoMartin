package ee.bcs.koolitus.Ylesanne2_Loomad;

abstract class Mammal {
	String speciesName;
	Gender gender;
	
	abstract void moveAhead();
	
	public Mammal defineSpeciesName(String speciesName) {
		this.speciesName = speciesName;
		return this;
		
	}
	public Mammal setGender(Gender gender) {
		this.gender = gender;
		return this;
	}
	
}
