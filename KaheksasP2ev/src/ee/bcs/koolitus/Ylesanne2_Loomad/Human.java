package ee.bcs.koolitus.Ylesanne2_Loomad;

public class Human extends Mammal{
	
	public boolean isStanding = false;
	
	@Override
	void moveAhead() {
		if(!isStanding) {
			standUp();
		}
		makeStepWithLeg(Leg.LEFT);
		makeStepWithLeg(Leg.RIGHT);

	}
	private void standUp() {
		System.out.println("Human stands up.");
	}
	
	private void makeStepWithLeg(Leg leg) {
		System.out.println("Human made a step ahead with " + leg.toString() + " leg.");
	}

}
