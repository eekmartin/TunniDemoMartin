package ee.bcs.koolitus.p2rilus;

public class Inimene {
	private String nimi;
	private int vanus;
	private String aadress;
	
	public void kysibKysimuse(String kysimus) {
		System.out.println("Küsib: " + kysimus);
	}
	
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public int getVanus() {
		return vanus;
	}
	public void setVanus(int vanus) {
		this.vanus = vanus;
	}
	public String getAadress() {
		return aadress;
	}
	public void setAadress(String aadress) {
		this.aadress = aadress;
	}
	
	@Override
	public String toString() {
		return "Nimi = " + nimi + "; vanus on " + vanus + "; aadress on " + aadress + "";
	}
	
	
}
