package ee.bcs.koolitus.p2rilus;

public class Lektor extends Inimene{
	private	boolean istekohtKlassiEes = true;
	
	public String raagib(String lause) {
		return "Lektor ütleb: \"" + lause + "\"";
	}
	
	public boolean hasIstekohtKlassiEes() {
		return istekohtKlassiEes;
	}
	
	public Lektor setIstekohtKlassiEes(boolean istekohtKlassiEes) {
		this.istekohtKlassiEes = istekohtKlassiEes;
		return this;
		
	}

	@Override
	public String toString() {
		return "Nimi = " + this.getNimi() + "; vanus on " + this.getVanus() + "; aadress on " + this.getAadress() + "";
	}
	
	

}
