package ee.bcs.koolitus.p2rilus.koopia2;

public class Lektor extends Inimene{
	private	boolean istekohtKlassiEes = true;
	
	public Lektor() {
		//"super" - sellega kutsun välja inimese klassi konstruktori:
		//super peab olema konstruktori esimene käsk!
		super("LektorX");
		System.out.println("Lektor on valmis tehtud.");
	}
		//alternatiiv: lektorile antakse kohe nimi ka:
	public Lektor(String nimi) {
		super(nimi);
	}
	
	public String raagib(String lause) {
		return "Lektor ütleb: \"" + lause + "\"";
	}
	
	public boolean hasIstekohtKlassiEes() {
		return istekohtKlassiEes;
	}
	
	public Lektor setIstekohtKlassiEes(boolean istekohtKlassiEes) {
		this.istekohtKlassiEes = istekohtKlassiEes;
		return this;
		
	}

	@Override
	public String toString() {
		return "Nimi = " + this.getNimi() + "; vanus on " + this.getVanus() + "; aadress on " + this.getAadress() + "";
	}
	
	

}
