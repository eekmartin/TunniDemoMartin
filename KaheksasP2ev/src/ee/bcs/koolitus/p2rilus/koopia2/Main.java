package ee.bcs.koolitus.p2rilus.koopia2;

public class Main {
	public static void main (String[] args) {
		Inimene inimene = new Inimene ("Mati");
		inimene.setNimi("Mati2");
		inimene.setVanus(22);
		inimene.setAadress("Aia 7");
		System.out.println(inimene);
		inimene.kysibKysimuse("Miks Päike tõuseb idast?");
		System.out.println("---------------");
		
		Inimene lektor1 = new Lektor();
		lektor1.setNimi("Henn");
		//kontrollin, kas lektor vastab tingimusele hasIstekohtKlassiEes:
		((Lektor) lektor1).setIstekohtKlassiEes(true);
		System.out.println("Lektor1: " + ((Lektor) lektor1).getNimi());
		//System.out.println("Lektor1: " + lektor1.getNimi());
		System.out.println("Lektor 1: " + ((Lektor) lektor1).hasIstekohtKlassiEes());
		System.out.println("------------------");
		System.out.println("Lektor 1: " + lektor1);
		lektor1.kysibKysimuse("Mis asi on .net?");
		System.out.println("------------------");
		Lektor lektor2 = new Lektor("Heleen");
		System.out.println("Lektor 2: " + lektor2);
		lektor2.kysibKysimuse("Mis asi on pärimine?");
		System.out.println(lektor2.raagib("Laiendava klassi nimele järgneb extends VanemaKlassiNimi"));
		System.out.println("Lektor 2: " + lektor2);
		System.out.println("------------------");
		
		Inimene opilane1 = new Opilane("Martin");
		opilane1.setNimi("Jüri");
		opilane1.setVanus(30);
		opilane1.setAadress("Wismari tn");
		System.out.println("Õpilane 1: " + opilane1);
		opilane1.kysibKysimuse("Kas võib koju minna?");
		

		
		
	}
	
}
