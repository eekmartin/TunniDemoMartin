package ee.bcs.koolitus.p2rilus.koopia2;

import java.util.ArrayList;
import java.util.List;

public class Opilane extends Inimene{
	
	public Opilane(String nimi) {
		super(nimi);
	}
		
		private	boolean istekohtKlassis = false;
		private List<String> markmed = new ArrayList<>();
		
		public void kuulab() {
			System.out.println("Õpilane " + this.getNimi() + " kuulab.");
		}
		
		@Override
		public void kysibKysimuse(String kysimus) {
			System.out.println("Õpilane ootab küsimiseks sobilikku hetke.");
			System.out.println("Õpilane küsib: " + kysimus);
		}
		
		public boolean hasIstekohtKlassis() {
			return istekohtKlassis;
		}
		
		public Opilane setIstekohtKlassis(boolean istekohtKlassis) {
			this.istekohtKlassis = istekohtKlassis;
			return this;
			
		}
		@Override
		public String toString() {
			return "Õpilane [Nimi = " + this.getNimi() + "; vanus on " + this.getVanus() + 
					"; aadress on " + this.getAadress() + "; // kas istub klassis? --> "
							+ "" + istekohtKlassis + "//" + " märkmed = " + markmed + "";
		}
		

}
