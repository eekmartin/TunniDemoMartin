package test.ee.bcs.koolitus.auto;

import org.junit.Test;

import ee.bcs.koolitus.auto.Auto;
import ee.bcs.koolitus.auto.K2igukast;
import ee.bcs.koolitus.auto.KytuseTyyp;
import ee.bcs.koolitus.auto.Mootor;
import org.junit.Assert;
import org.junit.Before;

public class AutoTest {
		Auto auto;
		//tekitan klassi-taseme muutuja - nähtav kõigis järgnevates meetodites
	
	@Before
	public void setUp() {
		//setUp käivitatakse järgneva kahe klassi testide puhul. Tekitab puhta seisu, millega teste läbi käia.
		auto = new Auto();
		Mootor mootor = new Mootor();
		mootor.setK2igukast(K2igukast.AUTOMAAT).setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(140);
		auto.setKohtadeArv(5).setMootor(mootor).setUsteArv(5);
	
	}
	@Test
	public void testNotStartedAfterCreation() {
		//nimi kirjeldab, mida testin. Korraga peaks testima ühte tegevust. Järgmine tegevus uude testi!
		Assert.assertEquals(false, auto.isMootorK2ib());
		//Assert

	}
	
	@Test
	public void testDiiselMootoriK2ivitamine(){
		Auto auto = new Auto();
		Mootor mootor = new Mootor();
		mootor.setK2igukast(K2igukast.AUTOMAAT).setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(140);
		auto.setKohtadeArv(5).setMootor(mootor).setUsteArv(5);
		Assert.assertEquals(false, auto.isMootorK2ib());
		
		auto.kaivitaMootor();
		Assert.assertEquals(true, auto.isMootorK2ib());
	}
	
	@Test
	public void testBensiiniMootoriK2ivitamine(){
		Auto auto = new Auto();
		Mootor mootor = new Mootor();
		mootor.setK2igukast(K2igukast.AUTOMAAT).setKytuseTyyp(KytuseTyyp.BENSIIN).setVoimsus(140);
		auto.setKohtadeArv(5).setMootor(mootor).setUsteArv(5);
		Assert.assertEquals(false, auto.isMootorK2ib());
		
		auto.kaivitaMootor();
		Assert.assertEquals(true, auto.isMootorK2ib());
	}
}
