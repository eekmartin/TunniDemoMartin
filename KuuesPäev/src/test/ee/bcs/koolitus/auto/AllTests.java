package test.ee.bcs.koolitus.auto;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	AutoTest.class
})
//siit suite'ist saan panna kõik testid korraga käima
public class AllTests {

}
