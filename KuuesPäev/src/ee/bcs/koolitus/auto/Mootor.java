package ee.bcs.koolitus.auto;

public class Mootor {
	int voimsus;
	KytuseTyyp kytuseTyyp;
	K2igukast k2igukast;

	public K2igukast getK2igukast() {
		return k2igukast;
	}

	public Mootor setK2igukast(K2igukast k2igukast) {
		this.k2igukast = k2igukast;
		return this;
	}

	public int getVoimsus() {
		return this.voimsus;
	}

	public Mootor setVoimsus(int voimsus) {
		this.voimsus = voimsus;
		return this;
		// this. rõhutab, et muudan kogu mootori objektoi muutujat voimsus
		// kui getteris this. juba olemas, ei pea setteris seda lisama?
	}

	public KytuseTyyp getKytuseTyyp() {
		return this.kytuseTyyp;
	}

	public Mootor setKytuseTyyp(KytuseTyyp kytuseTyyp) {
		// setMuutuja(MuutujaTüüp temaNimi)
		this.kytuseTyyp = kytuseTyyp;
		return this;

	}

	// minu klass "mootor" laiendab klassi object, kus on olemas "toString" meetod.
	// Kõik laiendajad pärivad vanem-klassi meetodi
	@Override
	public String toString() {
		return "Mootor: võimsus = " + this.voimsus + ", kütuse tüüp on " + this.kytuseTyyp;

	}
}
