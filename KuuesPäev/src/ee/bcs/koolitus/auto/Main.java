package ee.bcs.koolitus.auto;


public class Main {
	public static void koikAutodSoidavad(Auto... autod) {
		for(Auto auto:autod) {
			System.out.println("--------------");
			auto.soida();
			System.out.println("--------------");
		}
	}

	public static void main (String[] args) {
		Auto auto1 = new Auto();
		Mootor m1 = new Mootor();
		m1.setVoimsus(250)
			.setKytuseTyyp(KytuseTyyp.DIISEL)
			.setK2igukast(K2igukast.MANUAAL);
		auto1.setKohtadeArv(2)
			.setUsteArv(2)
			.setMootor(m1)
			.soida();
		System.out.println("Esimene auto sõidab.");
		auto1.vahetaK2iku(5);
		
		
		
			System.out.println("-----------------");
			
		
		Auto auto2 = new Auto();
		//sulud Auto järel "Auto()" on kasutusel konstruktori välja kutsumiseks
		// siin tegin klassis "Auto" kaks auto-objekti auto1 ja auto2	
		auto2.setKohtadeArv(5);
		auto2.setUsteArv(4);
		Mootor m2 = new Mootor();
		m2.setVoimsus(140);
		m2.setKytuseTyyp(KytuseTyyp.BENSIIN);
		m2.setK2igukast(K2igukast.AUTOMAAT);
		auto2.setMootor(m2);
		auto2.soida();
		System.out.println("Teine auto sõidab.");
		
		
	
		
		koikAutodSoidavad(auto1, auto2);
		
		
		
			}

		
		
	/*	
		System.out.println(m1);
		System.out.println(m2);
		
		System.out.println("------------------");
		
		System.out.println("Auto 1 on " + auto1);
		System.out.println("Auto 2 on " + auto2);
		
		System.out.println("------------------");
		
		Auto[] autod = {auto1, auto2};
		System.out.println("Massiivist : " +Arrays.deepToString(autod));
		
		System.out.println("------------------");
		List<Auto> autod2 = new ArrayList<>();
		autod2.add(auto1);
		autod2.add(auto2);
		System.out.println("Listist: " + autod2);
		*/
	}

