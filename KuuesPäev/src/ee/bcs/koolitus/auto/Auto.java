package ee.bcs.koolitus.auto;

import java.util.List;

public class Auto {
	int usteArv;
	int kohtadeArv;
	Mootor mootor;
	int k2ikHetkel=1;
	boolean mootorK2ib = false;
	int id;
	static int counter = 0;
	
	
	public Auto(){
		counter++;
		id = counter;
		
	}
	public Auto(Mootor mootor) {
		this.mootor = mootor;
		counter++;
		id = counter;
		
	}

		
	public void kaivitaMootor() {
		if(mootor.getKytuseTyyp().equals(KytuseTyyp.DIISEL)) {
			System.out.println("Eelsüüde alustatud");
			System.out.println("Eelsüüde lõpetatud");
		}
		this.mootorK2ib = true;
		System.out.println("Mootor käivitatud");
	}
	public void soida() {
		//kas mootor käib? Kui ei, siis käivita.
		if(!mootorK2ib) {
			System.out.println("Enne sõitma hakkamist käivita mootor!");
			kaivitaMootor();
		}else
		{
			System.out.println("Mootor käib.");
		}
		
		if(mootor.getK2igukast().equals(K2igukast.MANUAAL)) {
			System.out.println("Sidur alla, pane sisse esimene käik, vabasta sidur.");
			/*System.out.println("Pane sisse teine käik.");
			System.out.println("Pane sisse kolmas käik.");
			System.out.println("Pane sisse neljas käik.");
			System.out.println("Pane sisse viies käik.");
			*/
		}else
				{
			System.out.println("Lükka käigukang asendisse D. Vabasta piduripedaal.");

		}
	
	}
	public void vahetaK2iku(int maxK2ik) {
		if(k2ikHetkel<maxK2ik) {
			System.out.println("Sul on lülitatud " + k2ikHetkel + ". käik. Pane sisse " + (++k2ikHetkel) + " käik.");
			vahetaK2iku(maxK2ik);
		}
	}			
	
		//paremklõps - Source - Generate Getters/Setters (siis ei pea käsitsi kirjutama neid ükshaaval)
	public int getUstArv() {
		return usteArv;
	}
	/*
	public void setUsteArv(int ustArv) {
		this.usteArv = ustArv;
	}
	*/
	
	public Auto setUsteArv(int UsteArv) {
		this.usteArv = UsteArv;
		return this;
	}
	public int getKohtadeArv() {
		return kohtadeArv;
	}
	public Auto setKohtadeArv(int KohtadeArv) {
		this.kohtadeArv = KohtadeArv;
		return this;
	}
	public Mootor getMootor() {
		return mootor;
	}
	public Auto setMootor(Mootor mootor) {
		this.mootor = mootor;
		return this;
	}
	public boolean isMootorK2ib() {
		return this.mootorK2ib;
	}
		
		public int getId() {
		return id;

	}
	public String toString() {
		return counter + "Auto: uste arv = " + usteArv + ", kohtade arv on " + kohtadeArv + "; " + mootor;

		
		
	}

}
