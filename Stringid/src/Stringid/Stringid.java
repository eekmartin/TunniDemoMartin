package Stringid;

public class Stringid {

	public static void main (String[] args) {
		String text1 = "This is first text";
		String text2;
// väärtus peab olema antud enne, kui sa seda kasutama hakkad (ehk siis enne sysouti)
		String text3 = "";
		
		System.out.println("text1 = " + text1);
		text2 ="value given \tlater";
// \t teeb teksti sisse tabi (võib järgneva sõnaga ka kokku kirjutada)
		System.out.println("text2 = " + text2);
		System.out.println("text3 = " + text3);
		System.out.println("asi" == "asi");
		System.out.println("asi2" == ("a" + "si2"));
//siin saab liita, sest liidan pointerid?
		String text4 = "asi";
		String text5 = "a";
		String text6 = "si";
		System.out.println(text4 == (text5+text6));
//siin ei kehti liitmistehe, kuna muutujaid (stringidena) ei saa omavahel liita
		System.out.println(text4.equals(text5+text6));
//siin jälle kehtib
		
	}
}
