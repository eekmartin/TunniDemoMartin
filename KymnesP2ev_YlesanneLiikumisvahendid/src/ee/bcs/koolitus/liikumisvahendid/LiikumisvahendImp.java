package ee.bcs.koolitus.liikumisvahendid;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

// teeme klassi abstraktseks, kuna ei soovi siin veel luua soidab meetodit
public abstract class LiikumisvahendImp implements LiikumisvahendInt {
	private static int counter = 0;
	private int id;

	// Mapi <esimene element> on võti, key;
	// Mapi <teine element> on value, mis sinna paigutatakse - antud juhul inimene
	private Map<Integer, Inimene> omanikud = new HashMap<>();

	public LiikumisvahendImp(Inimene omanik) {
		counter++;
		id = counter;
		omanikud.put(omanik.getId(), omanik);
	}

	public int getId() {
		return this.id;
	}

	public Map<Integer, Inimene> getOmanikud() {
		return omanikud;
	}

	public void setOmanikud(Map<Integer, Inimene> omanikud) {
		this.omanikud = omanikud;
	
	}
	
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		return this.getId() == ((LiikumisvahendImp)obj).getId();
	}
	
	@Override
	public int hashCode() {
		int hash = 15;
		hash = 4858 * hash + Objects.hashCode(id);
		return hash;
	}
}
