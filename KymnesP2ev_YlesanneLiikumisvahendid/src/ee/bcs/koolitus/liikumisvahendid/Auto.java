package ee.bcs.koolitus.liikumisvahendid;

public class Auto extends Mootorsoiduk {
	private AutoTyyp autoTyyp;
	public Auto(Inimene omanik, String regNumber, JuhiloaKategooria noutudKategooria) {
		super(omanik, regNumber, noutudKategooria);
		// TODO Auto-generated constructor stub
	}

	public AutoTyyp getAutoTyyp() {
		return autoTyyp;
	}
	
	public Auto setAutoTyyp(AutoTyyp autoTyyp) {
		this.autoTyyp = autoTyyp;
		return this;
	}

	@Override
	public String toString() {
		return "Auto [autoTyyp=" + autoTyyp + ", getRegNumber()=" + getRegNumber() + ", getNoutudJuhiloaKategooria()="
				+ getNoutudJuhiloaKategooria() + ", getId()=" + getId() + ", getOmanikud()=" + getOmanikud() + "]";
	}

}
