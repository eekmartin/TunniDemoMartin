package ee.bcs.koolitus.liikumisvahendid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Inimene {
	
	//counteri teeme selleks, et saaksime objektidele ID-sid anda
	private static int counter=0;
	private int id;
	private String name;
	private String surname;
	private Date birthday;
	private List<JuhiloaKategooria> juhiloaKategooriad = new ArrayList<>();
	
	public Inimene() {
		counter++;
		id = counter;
	}
	
	//siin teeme ID-le ainult getteri
	//setterit ei tee, et keegi seda muuta ei saaks hiljem
	public int getId() {
		return id;
	}


	public List<JuhiloaKategooria> getJuhiloaKategooriad() {
		return juhiloaKategooriad;
	}
	public void setJuhiloaKategooriad(List<JuhiloaKategooria> juhiloaKategooriad) {
		this.juhiloaKategooriad = juhiloaKategooriad;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	//kontrollime, kas inimesel olevate kategooriate loetelus on juba sama kategooria olemas
	public List<JuhiloaKategooria> lisaJuhiloaKategooria(JuhiloaKategooria kategooria){
		if(!this.juhiloaKategooriad.contains(kategooria)) {
		this.juhiloaKategooriad.add(kategooria);
		}
		return this.juhiloaKategooriad;
	}
	
	@Override
	public String toString() {
		return "Inimene [" + name + " " + surname + ", birthday=" + birthday + ", juhiloaKategooriad="
				+ juhiloaKategooriad + "]";
	}
	

}
