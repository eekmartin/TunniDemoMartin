package ee.bcs.koolitus.liikumisvahendid;

public class Mootorratas extends Mootorsoiduk {

	public Mootorratas(Inimene omanik, String regNumber, JuhiloaKategooria noutudKategooria) {
		super(omanik, regNumber, noutudKategooria);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Mootorratas [getRegNumber()=" + getRegNumber() + ", getNoutudJuhiloaKategooria()="
				+ getNoutudJuhiloaKategooria() + ", getId()=" + getId() + ", getOmanikud()=" + getOmanikud() + "]";
	}

	

}
