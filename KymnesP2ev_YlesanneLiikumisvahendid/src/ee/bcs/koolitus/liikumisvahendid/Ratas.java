package ee.bcs.koolitus.liikumisvahendid;

public class Ratas extends LiikumisvahendImp implements Comparable<Ratas>{

	private int k2ikudeArv;
	private String seeriaNr;
	private Istumisasend istumisasend;

	public Ratas(Inimene omanik) {
		super(omanik);
	}

	public Istumisasend getIstumisasend() {
		return istumisasend;
	}

	public Ratas setIstumisasend(Istumisasend istumisasend) {
		this.istumisasend = istumisasend;
		return this;
	}

	public int getK2ikudeArv() {
		return k2ikudeArv;
	}

	public void setK2ikudeArv(int k2ikudeArv) {
		this.k2ikudeArv = k2ikudeArv;
	}

	public String getSeeriaNr() {
		return seeriaNr;
	}

	public void setSeeriaNr(String seeriaNr) {
		this.seeriaNr = seeriaNr;
	}

	@Override
	public String toString() {
		return "Ratas [k2ikudeArv=" + k2ikudeArv + ", seeriaNr=" + seeriaNr + ", istumisasend=" + istumisasend + ", Id="
				+ getId() + ", Omanikud=" + getOmanikud() + "]";
	}

	@Override
	public void soidab(String algus, String lopp) {
		// TODO Auto-generated method stub

	}

	@Override
	public int compareTo(Ratas otherRatas) {
		if(this.equals(otherRatas)) {
		return 0;
	}
		if(this.seeriaNr.compareTo(otherRatas.seeriaNr) !=0) {
			return this.seeriaNr.compareTo(otherRatas.seeriaNr);
		}else if(this.istumisasend.compareTo(otherRatas.istumisasend) !=0) {
			
		}else if(this.k2ikudeArv < otherRatas.k2ikudeArv) {
			return -1;
		}else if(this.k2ikudeArv > otherRatas.k2ikudeArv) {
			return 1;
		}
		return 0;
	}

}
