package ee.bcs.koolitus.liikumisvahendid;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {
	public static void main(String[] args) throws ParseException {

		// siin kasutame Date formaati, anname selle kaudu ette, kuidas kuupäeva kuvada
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

		// INIMENE
		Inimene inimene1 = new Inimene();
		inimene1.setName("Eesnimi");
		inimene1.setSurname("Perenimi");
		inimene1.setBirthday(formatter.parse("22.02.2002"));
		inimene1.lisaJuhiloaKategooria(JuhiloaKategooria.B);
		inimene1.lisaJuhiloaKategooria(JuhiloaKategooria.A);
		System.out.println(inimene1);

		Inimene inimene2 = new Inimene();
		inimene2.setName("Mootorrattur");
		inimene2.setSurname("Hiir");
		inimene2.setBirthday(formatter.parse("01.01.2001"));
		inimene2.lisaJuhiloaKategooria(JuhiloaKategooria.B);
		System.out.println(inimene2);

		Inimene inimene3 = new Inimene();
		inimene3.setName("Onu");
		inimene3.setSurname("Kaubikujuht");
		inimene3.setBirthday(formatter.parse("03.03.2003"));
		inimene3.lisaJuhiloaKategooria(JuhiloaKategooria.C);
		System.out.println(inimene3);

		Set<LiikumisvahendImp> soidukid = new HashSet<>();

		soidukid.add(new Rula(inimene3));
		soidukid.add(new Auto(inimene2, "321FGH", JuhiloaKategooria.B1));
		soidukid.add(new Auto(inimene1, "456IJK", JuhiloaKategooria.C));
		soidukid.add(new Veesoiduk(inimene1, "12MNB", JuhiloaKategooria.LAEV));

		for (LiikumisvahendImp liikumisvahend : soidukid) {
			System.out.println(liikumisvahend);
		}

		Set<Ratas> rattad = new TreeSet();
		for (LiikumisvahendImp lv : soidukid) {
			if (lv instanceof Ratas) {
				rattad.add((Ratas) lv);
			}

		}

		// //RATAS1
		// Ratas ratas = new Ratas(inimene1);
		// ratas.setK2ikudeArv(24);
		// ratas.setIstumisasend(Istumisasend.PÜSTINE);
		//
		//// //RULA
		//// Rula rula = new Rula();
		//
		//
		// //AUTO
		// Auto auto = new Auto(inimene3, null, null);
		// auto.setAutoTyyp(AutoTyyp.KAUBIK);
		// auto.setRegNumber("123ABC");
		// auto.setNoutudJuhiloaKategooria(JuhiloaKategooria.C1);
		//
		// //MOOTORRATAS
		// Mootorratas mootorratas = new Mootorratas(inimene2, null, null);
		// mootorratas.setRegNumber("456XY");
		// mootorratas.setNoutudJuhiloaKategooria(JuhiloaKategooria.A);
		//
		// //JAHT
		// Veesoiduk veesoiduk = new Veesoiduk (inimene1, null, null);
		// veesoiduk.setVeesoidukiTyyp(VeesoidukiTyyp.JAHT);
		// veesoiduk.setRegNumber("1234567890");
		// veesoiduk.setNoutudJuhiloaKategooria(JuhiloaKategooria.LAEV);
		//
		// System.out.println("Prindi sõidukid: " + auto + ratas + mootorratas +
		// veesoiduk);
	}
}
