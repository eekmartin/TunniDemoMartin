package ee.bcs.koolitus.liikumisvahendid;

public class Veesoiduk extends Mootorsoiduk {

	private VeesoidukiTyyp veesoidukiTyyp;
	
	@Override
	public String toString() {
		return "Veesoiduk [veesoidukiTyyp=" + veesoidukiTyyp + ", getRegNumber()=" + getRegNumber()
				+ ", getNoutudJuhiloaKategooria()=" + getNoutudJuhiloaKategooria() + ", getId()=" + getId()
				+ ", getOmanikud()=" + getOmanikud() + "]";
	}

	public Veesoiduk(Inimene omanik, String regNumber, JuhiloaKategooria noutudKategooria) {
		super(omanik, regNumber, noutudKategooria);
		// TODO Auto-generated constructor stub
	}

	public VeesoidukiTyyp getVeesoidukiTyyp() {
		return veesoidukiTyyp;
	}

	public Veesoiduk setVeesoidukiTyyp(VeesoidukiTyyp veesoidukiTyyp) {
		this.veesoidukiTyyp = veesoidukiTyyp;
		return this;
	}
}
