package ee.bcs.koolitus.liikumisvahendid;

public enum VeesoidukiTyyp {
	PAAT, LAEV, JAHT, PURJEKAS;
}
