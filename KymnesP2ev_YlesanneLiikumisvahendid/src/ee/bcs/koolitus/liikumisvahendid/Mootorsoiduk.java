package ee.bcs.koolitus.liikumisvahendid;

public class Mootorsoiduk extends LiikumisvahendImp {

	private String regNumber;
	private JuhiloaKategooria noutudJuhiloaKategooria;
	
	public Mootorsoiduk(Inimene omanik, String regNumber, JuhiloaKategooria noutudKategooria) {
		super(omanik);
		this.regNumber = regNumber;
		this.setNoutudJuhiloaKategooria(noutudKategooria);
		
	}
	@Override
	public void soidab(String algus, String lopp) {
		// TODO Auto-generated method stub
	}
	 public boolean kaivitub() {
	 return true;
	 }
	 public boolean seiskub() {
	 return true;
	 }

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}
	public JuhiloaKategooria getNoutudJuhiloaKategooria() {
		return noutudJuhiloaKategooria;
	}
	public void setNoutudJuhiloaKategooria(JuhiloaKategooria noutudJuhiloaKategooria) {
		this.noutudJuhiloaKategooria = noutudJuhiloaKategooria;
	
	}

	

}
