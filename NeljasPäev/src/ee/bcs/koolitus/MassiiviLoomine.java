package ee.bcs.koolitus;

import java.util.Arrays;

public class MassiiviLoomine {
	public static void main(String[] args) {
		//alternatiiv 1
		String[][] tabel = {{ "Mari", "25", "Tallinn", "5123456"}, {"Kati", "28", "Tartu"}, {"Mati", "30", "Pärnu", "5213465"}};
		System.out.println("Alternatiiv 1: " + Arrays.deepToString(tabel));		
		System.out.println("--------");
		
		
		//alternatiiv 2
		String[][] tabel1 = new String[3][4];
		String[] mari = {"Mari", "25", "Tallinn", "5123456"};
		String[] kati = {"Kati", "28", "Tartu"};
		String[] mati = {"Mati", "30", "Pärnu", "5213465"};
		tabel1[0] = mari;
		tabel1[1] = kati;
		tabel1[2] = mati;
		System.out.println("Alternatiiv 2: " + Arrays.deepToString(tabel1));
		System.out.println("--------");


		String[][] tabel3 = new String[4][4];
		String[] riho = {"Riho", "24", "Tallinn", "5321465"};
		tabel3[0] = tabel1[0];
		tabel3[1] = tabel1[1];
		tabel3[2] = tabel1[2];
		tabel3[3] = riho;
		
		System.out.println("Ülesanne 2: " + Arrays.deepToString(tabel3));
		System.out.println("--------");

		
		String[][] tabel4 = new String[4][4];
		System.arraycopy(tabel, 0, tabel4, 0, 3);
		tabel4[3] = riho;
		System.out.println("Ülesande 2 alternatiiv 2: " + Arrays.deepToString(tabel4));
		System.out.println("------");
		
		int i = 3;
		for(; i<=5; i++){
			System.out.println("i = " +i);
		}
		System.out.println("----------------");
				
		for(int rida = 0; rida<tabel4.length; rida++){
			for(int veerg = 0; veerg < tabel4[rida].length; veerg++){
				System.out.print(tabel4[rida][veerg] + ";");
			}
			System.out.println();
			}
		System.out.println("--------------------");
		
		String[][] tabel5 = new String[5][5];
		String[] tiit = {"Tiit", "38", "Tartu", "5689458", "programmeerija"};
			for(int inimesePos = 0; inimesePos<tabel4.length; inimesePos++){
			tabel5[inimesePos] = tabel4[inimesePos];
			}
			tabel5[4] = tiit;

				System.out.println("Ülesanne 3: " + Arrays.deepToString(tabel5));
				System.out.println("--------------------");

	
		int j = 4;
		while(j<=5) {
			System.out.println("j = " + j);
			j++;
		}
		
		int k = 6;
		do {
			System.out.println("k = " + k);
			k++;
		} while(k<=5);
		System.out.println("--------------------");
				
		boolean kasOnKymme = false;
		int l = 9;
		while(l<10) {
			l++;
			if(l==10)
				kasOnKymme = true;
			System.out.println(kasOnKymme);
		}
		System.out.println("---------");
		
		boolean kasOnKymme1 = false;
		int m = 7;
		do {
			System.out.println(kasOnKymme1);
			m++;
			if(m==10)
				kasOnKymme = true;
		} while(m<10);
		
		boolean kasOnSada = false;
		while (kasOnSada) {
			int arv = 100;
			System.out.println("arv oli tsükli alguses " + arv );
			arv = arv - 10; 
				if (arv == 10) {
					kasOnSada = true;
				}
			System.out.println("arv on tsükli lõpus " + arv);
		}
		System.out.println("Lõpetas tsükli");
		System.out.println("----------------");
		
		for (String ajutine[]: tabel5) {
			for (int n = 0; n < ajutine.length; n++) {
				System.out.print(ajutine[n] + "; ");
				
			}
			System.out.println();
		}
				
		
		}	
}

