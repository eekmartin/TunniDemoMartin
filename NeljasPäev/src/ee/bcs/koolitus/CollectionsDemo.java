package ee.bcs.koolitus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Hashtable;

public class CollectionsDemo {
	public static void main(String[] args) {
		List<String> nimed = new ArrayList<>();
		
		nimed.add("Mari");
		nimed.add("Kati");
		nimed.add("Mati");
		System.out.println(nimed);
		
		System.out.println("----------------");
		
		List<String> nimed2 = new ArrayList<>();
		nimed.add(1, "Jüri");
		nimed.add("Rain");
		//1 on siin positsioon, kuhu uus nimi lisatakse (0, 1, 2 jne). 
		//Seejuures pärast seda pos olnud nimed liiguvad edasi
		nimed.addAll(2, nimed2);
		System.out.println(nimed);
		System.out.println(nimed.indexOf("Jüri"));
		
		System.out.println("-----------------");
		
		String t1 = "Jü";
		String t2 = "ri";
		String t3 = t1 + t2;
		System.out.println(nimed.indexOf(t3));
		
		System.out.println("-----------------");
		
		nimed.set(4, "Kadri");
		System.out.println(nimed);
		System.out.println("-----------------");
		
		nimed.remove("Kati");
		System.out.println(nimed);
		System.out.println("-----------------");
		
		
		List<String[]> lemmikloomad = new ArrayList<>();
		String[] muki = {"Muki", "koer"};
		String[] jõmmu = {"Jõmmu", "kass"};
		String[] lexus = {"Lexus", "koer"};
		String[] maki = {"Maki", "koer"};
		String[] alice = {"Alice", "kass"};

		lemmikloomad.addAll(Arrays.asList(muki, jõmmu, lexus, maki, alice));
		for(String[] lemmik : lemmikloomad)

		System.out.println(Arrays.deepToString(lemmik));
		System.out.println(lemmikloomad.indexOf(lexus));
		System.out.println("----------------");
		
		
		Map<String, String> inimesed = new Hashtable<>();
		inimesed.put("38803032222", "Jann");
		inimesed.put("38703035555", "Ain");
		inimesed.put("38603034444", "Raivo");

		
		for (String ab : inimesed.values()) System.out.println(ab);
		System.out.println("------------");
		
		for (String ab : inimesed.keySet()) 		
			System.out.println(String.format("Võti %s on väärtusega %s", ab, inimesed.get(ab)));

		
	}

}
