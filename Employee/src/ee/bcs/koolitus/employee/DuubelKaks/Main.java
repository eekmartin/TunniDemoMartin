package ee.bcs.koolitus.employee.DuubelKaks;

import java.math.BigDecimal;

public class Main {
	
		static Employee changeSalary(Employee employee) {
			
			//võib nii:
			//employee = changeSalary(employee, BigDecimal.valueOf(100));
			//return employee;
			//või nii:
			
			return changeSalary(employee, BigDecimal.valueOf(100));
		}
		static Employee changeSalary(Employee employee, BigDecimal higherSalary) {
			BigDecimal newSalary = employee.getSalary().add(higherSalary);
			employee.setSalary(newSalary);
			return employee;
		}
		
		public static void main (String[] args) {
			Employee emp = new Employee();
			emp.setSalary(BigDecimal.valueOf(1_000));
			emp = changeSalary(emp);
			System.out.println("Esimene palgatõus: " + emp.getSalary());
			
			emp = changeSalary(emp, BigDecimal.TEN);
			System.out.println("Teine palgatõus: " + emp.getSalary());
 
		
	}

}
