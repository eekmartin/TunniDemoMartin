package ee.bcs.koolitus.employee;

import java.math.BigDecimal;

public class Main {
	
		static Employee changeSalary(Employee employee) {
			BigDecimal newSalary = employee.getSalary().add(BigDecimal.valueOf(100));
			employee.setSalary(newSalary);
			return employee;
		}
		static Employee changeSalary(Employee employee, BigDecimal salary) {
			BigDecimal newSalary = employee.getSalary().add(salary);
			employee.setSalary(newSalary);
			return employee;
		}
		
		public static void main (String[] args) {
			Employee emp = new Employee();
			emp.setSalary(BigDecimal.valueOf(1_000));
			emp = changeSalary(emp);
			System.out.println("Esimene palgatõus: " + emp.getSalary());
			
			emp = changeSalary(emp);
			System.out.println("Teine palgatõus: " + emp.getSalary());
 
 
		
	}

}
