package ee.bcs.koolitus.kasutaja;


public class Main {

	public static void main(String[] args) {
		PlainDate start = PlainDate.of(1996, 2, 29);
		PlainDate end = PlainDate.of(2014, 2, 28);
		end = SystemClock.inLocalView().today();
		long years = CalendarUnit.YEARS.between(start, end);
		System.out.println(years);

	}

}
