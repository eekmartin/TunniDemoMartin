package ee.bcs.koolitus.kasutaja;

import java.util.Date;

public class Kasutaja {
	private int id;
	private String eesnimi;
	private String perenimi;
	private Date doe;
	private String osakond;



	public int getId() {
		return id;
	}
	public Kasutaja setId(int id) {
		this.id = id;
		return this;
	}
	public String getEesnimi() {
		return eesnimi;
	}
	public Kasutaja setEesnimi(String eesnimi) {
		this.eesnimi = eesnimi;
		return this;
	}
	public String getPerenimi() {
		return perenimi;
	}
	public Kasutaja setPerenimi(String perenimi) {
		this.perenimi = perenimi;
		return this;
	}
	public Date getDoe() {
		return doe;
	}
	public void setDoe(Date doe) {
		this.doe = doe;
	}
	public String getOsakond() {
		return osakond;
	}
	public void setOsakond(String osakond) {
		this.osakond = osakond;
	}
	@Override
	public String toString() {
		return "Kasutaja [id=" + id + ", eesnimi=" + eesnimi + ", perenimi=" + perenimi + ", doe=" + doe + ", osakond="
				+ osakond + "]";

	}

}
