package ee.bcs.koolitus.main;

import ee.bcs.koolitus.nahtavus.Nahtavus;
import ee.bcs.koolitus.nahtavus.Nahtavus2;
import ee.bcs.koolitus.nahtavus.NahtavuseAlamklassSamasKaustas;
import ee.bcs.koolitus.nahtavus.v2iksem.NahtavuseAlamklass;

public class Main {
	public static void main (String[] args) {
		NahtavuseAlamklass temaNimi = new NahtavuseAlamklass();
		temaNimi.testiNahtavusMuutujaid();
		
		NahtavuseAlamklassSamasKaustas temaNimi2 = new NahtavuseAlamklassSamasKaustas();
		temaNimi2.testiNahtavusMuutujaid();
		
		Nahtavus2 temaNimi3 = new Nahtavus2();
		temaNimi3.testiNahtavusMuutujaid();
	}
	public void testiNahtavuseMuutujaidJaMeetodeid() {
		Nahtavus nahtavus = new Nahtavus();
		//System.out.println(nahtavus.klassiMuutujaPrivate);
		//System.out.println(nahtavus.klassiMuutujaDefault);
		//System.out.println(nahtavus.klassiMuutujaProtected);
		//System.out.println(nahtavus.klassiMuutujaPublic);
	}

}
