package ee.bcs.koolitus.lemmikloom;


public class Main {
	public static void main (String[] args) {
		Lemmikloom alice = new Lemmikloom("Alice");
		alice.setLiik(Liik.KASS);
		alice.setSynnip2ev(alice.synnip2ev);
		alice.setJalgadeArv(4);
		
		Lemmikloom leksus = new Lemmikloom("Leksus", "27.06.2016", Liik.KOER);
		//seda varianti kasutan siis, kui väärtused on kohe teada. 
		//Kui pole teada (sisustatakse hiljem), siis kasutan eelmist varianti?
		leksus.setJalgadeArv(4);
		
		System.out.println(alice);
		System.out.println(leksus);
		
		System.out.println("Lemmikud : " + Lemmikloom.lemmikloomad);

		
	}
	

}
