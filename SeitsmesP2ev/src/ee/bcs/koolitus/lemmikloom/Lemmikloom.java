package ee.bcs.koolitus.lemmikloom;

import java.util.ArrayList;
import java.util.List;

public class Lemmikloom {
	static int counter = 0;
	public static final List<Lemmikloom> lemmikloomad = new ArrayList<>();
	int id;
	int jalgadeArv;
	protected String synnip2ev;
	//kui paned "protected" ette, siis muutub nähtavaks teises klassis?
	String nimi;
	Liik liik;
	
	public Lemmikloom(String nimi) {
		counter = counter + 10;
		id = counter;
		this.nimi = nimi;
		lemmikloomad.add(this);
	}
	public Lemmikloom(String nimi, String synnip2ev, Liik liik) {
		this(nimi);
		this.synnip2ev = synnip2ev;
		this.liik = liik;
	}
	
	public int getJalgadeArv() {
		return jalgadeArv;
	}
	public void setJalgadeArv(int jalgadeArv) {
		this.jalgadeArv = jalgadeArv;
	}
	public String getSynnip2ev() {
		return synnip2ev;
	}
	public void setSynnip2ev(String synnip2ev) {
		this.synnip2ev = synnip2ev;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public int getId() {
		return id;
	}
	public Liik getLiik() {
		return liik;
	}
	public void setLiik(Liik liik) {
		this.liik = liik;
	}
	@Override
	public String toString() {
		return "Lemmikloom [id=" + id + ", jalgadeArv=" + jalgadeArv + ", synnip2ev=" + synnip2ev + ", nimi=" + nimi + ", liik=" + liik
				+ "]";
		//ilma toStringita juhtub mis? Ei prindita neid väljasid enam välja main meetodis.
	}
	

}
