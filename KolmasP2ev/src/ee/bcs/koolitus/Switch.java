package ee.bcs.koolitus;

public class Switch {
	public static void main(String[] args) {
		String color = args[0].toLowerCase();
			if (color.equals("green"))
				System.out.println("Driver can drive the car.");
			else if (color.equals("yellow"))
				System.out.println("Driver has to be ready to stop the car or to start driving.");
			else if (color.equals("red"))
				System.out.println("Driver has to stop the car and wait for green light.");
			else
				System.out.println("Traffic lights are broken. Please follow the signs.");
			System.out.println("---------");
			
			String color1 = args[1].toLowerCase();	
		switch (color1) {
			case "green":
				System.out.println("Driver can drive the car.");
				break;
			case "yellow":
				System.out.println("Driver has to be ready to stop the car or to start driving.");
				break;
			case "red":
				System.out.println("Driver has to stop the car and wait for green light.");
				break;
			default:
				System.out.println("Traffic lights are broken. Please follow the signs.");
				System.out.println("---------");
			
		}
		
	}
}
