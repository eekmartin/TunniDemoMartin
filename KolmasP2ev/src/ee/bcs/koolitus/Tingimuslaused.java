package ee.bcs.koolitus;

public class Tingimuslaused {
	public static void main(String[] args) {
		int arv1 = Integer.parseInt(args[0]);
		int arv2 = Integer.parseInt(args[1]);
	if(arv1%2 == 0) {
		System.out.println(arv1 + " on paaris arv.");
	} else {
		System.out.println(arv1 + " ei ole paaris arv.");
	}
	System.out.println("----------");

	String kasArvOnPaaris = (arv2%2 == 0) ? ("Arv " + arv2 + " on paaris.") : ("Arv " + arv2 + " ei ole paaris.");
		System.out.println(kasArvOnPaaris);
	}
}
