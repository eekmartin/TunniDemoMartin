package ee.bcs.koolitus;

	public class StringBuilder1 {
		public static void main(String[] args) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("This is the first sentence. \n");
			stringBuilder.append("This is the second sentence added with append. \n");
			int thirdBeginning = stringBuilder.length();
			stringBuilder.insert(thirdBeginning, 
					"This is third sentence added by using insert, is added to the end of string buffer. \n");
			String neljaslause = "This is fourth sentence that goes before third sentence as using same position thirdBeginning. \n";
			stringBuilder.insert(thirdBeginning,  neljaslause);
			System.out.println(stringBuilder);
		}
}
