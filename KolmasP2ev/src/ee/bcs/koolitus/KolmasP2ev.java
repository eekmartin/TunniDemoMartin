package ee.bcs.koolitus;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author eekmartin
 *
 */
// sellega näitab igas (seotud?) projekti failis, kus seda klassi kasutatakse,
// et selle tegi eekmartin

public class KolmasP2ev {
	public static void main(String[] args) {
		// .format
		String stringToFormat = "%s. %s is %s years old. %5$s years old is %4$s. %<s is older than %2$s.";
		System.out.println(String.format(stringToFormat, "An elderly couple", "Mari", 72, "Jüri", 69));
		// Võib ka ühe reaga: System.out.println("%s. %s is %s years old. %5$s years old
		// is %4$s. %<s is older than %2$s", "An elderly couple", "Mari", 72, "Jüri",
		// 69));
		System.out.println("----------");

		// .split
		String textContainingNamesOfClassmates = "These are my classmates: Piret, Argo, Raiko, Andres, Anni, Maili, Kalev.";
		String textWithNamesOnly = textContainingNamesOfClassmates.split(":")[1];
		String[] arrayOfClassmates = textWithNamesOnly.split(",");
		// nurksulgudega tekitan massiivi - sysout kehtib siin kogu massiivi kohta. Kui
		// seda loogeliste sulgudega ei eralda?
		for (String classMates : arrayOfClassmates) {
			// for on massiivi tsükliga läbikäimine (hakkab tabelist rida-haaval muutujaid
			// võtma)
			System.out.println(classMates.replace(".", ""));
			// Sellega asendame punkti mittemillegagi: replace(".", ""), et loetelu viimasel
			// nimel poleks punkti
		}
		System.out.println("------");

		// .Scanner
		String scanneriTekst = "Name: Heleen Maibak Name: Henn Sarv Name: Mari Maasikas";

		// Separates by spaces
		Scanner scanner = new Scanner(scanneriTekst);
		//new käsk loob objekti, mis on valitud tüüpi
		while (scanner.hasNext()) {
			System.out.println(scanner.next());
		}
		System.out.println("------------");
		scanner.close();
		
		//Separates by given word
		Scanner scannerByWord = new Scanner(scanneriTekst);
		scannerByWord.useDelimiter("Name:");
		while (scannerByWord.hasNext()) {
			System.out.println(scannerByWord.next());
		}
		System.out.println("---------------------------");
		scannerByWord.close();
		
		//Reads text from file and separates by word "Name:"
		try (Scanner scannerFile = new Scanner(new File("TestFileScannerile.txt")).useDelimiter("Name:  ")) {
			while (scannerFile.hasNext()) {
				System.out.println(scannerFile.next());
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(KolmasP2ev.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
